#include <stdio.h>
#include <stdlib.h>


// Prototipos de funciones
// -----------------------
char **crear_matriz(int f, int c);
void mostrar_matriz(char **matriz, int f, int c);
void pedir_datos_matriz(char **matriz, int f, int c, char pieza);
void rellena_matriz(char **matriz, int f, int c, char car);
int comprobar_juego(char **matriz, int f, int c);


// Funcion main
// ------------
int main(){


    char **matriz, f=3, c=3;
    int cont = 0;

    matriz = crear_matriz(f,c);
    rellena_matriz(matriz, f, c, '-');

    do{

        if (cont % 2 == 0){
            system("clear");
            mostrar_matriz(matriz, f,c);
            printf("Turno del jugador X\n");
            pedir_datos_matriz(matriz, f,c, 'X');
        }else{
            system("clear");
            mostrar_matriz(matriz, f,c);
            printf("Turno del jugador O\n");
            pedir_datos_matriz(matriz, f,c, 'O');
        }

        cont ++;

    }while(comprobar_juego(matriz,f,c));

    mostrar_matriz(matriz,f,c);
    printf("\n\n\n\n");

    /*
    pedir_datos_matriz(matriz, f,c, 'X');
    mostrar_matriz(matriz, f,c);
    comprobar_juego(matriz,f,c);

    pedir_datos_matriz(matriz, f,c, 'X');
    mostrar_matriz(matriz, f,c);
    comprobar_juego(matriz,f,c);

    pedir_datos_matriz(matriz, f,c, 'X');
    mostrar_matriz(matriz, f,c);
    comprobar_juego(matriz,f,c);
    */

    return 0;

}



// Funciones del programa
// ----------------------



char **crear_matriz(int f, int c){

    char **matriz;
    int i;

    matriz = (char **)malloc(f*sizeof(char *));
    if (matriz == NULL){
        perror("No se ha podido reservar memoria dinamica con las filas.\n");
        exit(1);
    }else{

        for (i=0; i<f; i++){
            matriz[i] = (char *)malloc(c*sizeof(char));
            if (matriz[i]==NULL){
                perror("No se ha podido reservar memoria dinamica con las columnas.\n");
                exit(1);
            }
        }
    }

    return matriz;

}

void rellena_matriz(char **matriz, int f, int c, char car){

    int i, j;

    for (i=0; i<f; i++){
        for(j=0; j<c; j++){
            matriz[i][j] = car;
        }
    }
}


void pedir_datos_matriz(char **matriz, int f, int c, char pieza){

    int op1, op2, fin_while=1;

    do{
        do{
            do{
                fpurge(stdin);
                printf("Dame coordenada x: ");
                scanf("%d", &op1);
            }while(op1 < 0 || op1 > c);

            fpurge(stdin);
            printf("Dame coordenada y: ");
            scanf("%d", &op2);

        }while(op2 < 0 || op2 > f);

        if(matriz[op1][op2] == '-'){
            matriz[op1][op2] = pieza;
            fin_while = 0;
        }else{
            printf("Ya tiene un valor en ese campo.\n");
        }

    }while(fin_while);
}


void mostrar_matriz(char **matriz, int f, int c){

    for(int i=0; i<f; i++){
        for (int j=0; j<c; j++){
            printf("%c  ", matriz[i][j]);
        }
        printf("\n");
    }
}

int comprobar_juego(char **matriz, int f, int c){

    int i,j,cont_X, cont_O, winX=1, winO=1;

    // Comprobacion por filas
    // ----------------------

    for (i=0; i<f; i++){

        cont_X = 0;
        cont_O = 0;

        for (j=0; j<c; j++){
            
            if (matriz[i][j] == 'X'){
                cont_X ++;
            }
            
            if (matriz[i][j] == 'O'){
                cont_O ++;
            }
        }

        if (cont_X == 3){
            winX = 0;
            printf("Ha ganado el jugador X\n");
        }

        if (cont_O == 3){
            winO = 0;
            printf("Ha ganado el jugador O\n");
        }
    }

    // Comprobacion por columnas
    // -------------------------

    for (i=0; i<f; i++){

        cont_X = 0;
        cont_O = 0;

        for (j=0; j<c; j++){
            
            if (matriz[j][i] == 'X'){
                cont_X ++;
            }
            
            if (matriz[j][i] == 'O'){
                cont_O ++;
            }
        }

        if (cont_X == 3){
            winX = 0;
            printf("Ha ganado el jugador X\n");
        }

        if (cont_O == 3){
            winO = 0;
            printf("Ha ganado el jugador O\n");
        }
    }


    // Primera diagonal
    // ----------------

    cont_O = 0;
    cont_X = 0;

    for (i=0; i<f; i++){
        
        if (matriz[i][i] == 'X'){
            cont_X ++;
        }
            
        if (matriz[i][i] == 'O'){
            cont_O ++;
        }

    }

    if (cont_X == 3){
        winX = 0;
        printf("Ha ganado el jugador X\n");
    }

    if (cont_O == 3){
        winO = 0;
        printf("Ha ganado el jugador O\n");
    }

    // Segunda diagonal
    // ----------------

    cont_O = 0;
    cont_X = 0;
    i=0;

    for (j=c-1; j>=0; j--){

        if (matriz[i][j] == 'X'){
            cont_X ++;
        }
            
        if (matriz[i][j] == 'O'){
            cont_O ++;
        }

        i++;
    }

    if (cont_X == 3){
        winX = 0;
        printf("Ha ganado el jugador X\n");
    }

    if (cont_O == 3){
        winO = 0;
        printf("Ha ganado el jugador O\n");
    }


    if (winX == 1 && winO == 1){

        return 1;

    }else{

        return 0;
    }

}





